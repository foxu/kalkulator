﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp18
{
    public partial class Form1 : Form
    {
        decimal wynik, liczba, znak = 0, zero = 0;
        decimal podstawapercent;
        int klikniecia = 0;
        bool test;
        string ostatniaAkcja;
        public Form1()
        {
            InitializeComponent();
        }

#region "Działania"
        private void btnAdd_Click(object sender, EventArgs e)
        {
            ++klikniecia;
            if (klikniecia > 1)
                {
                    if (string.IsNullOrWhiteSpace(Input.Text))
                 {
                    
                }
                else
                 {
                    if (Char.IsNumber(Input.Text[Input.TextLength - 1]))
                    {
                        //Podstawapercent zapamietuje liczbe z ktorej ma liczyc pozniej procent
                        podstawapercent = decimal.Parse(Input.Text);
                        wynik += decimal.Parse(Input.Text);
                            label1.Text = wynik.ToString();
                            ostatniaAkcja = "+";
                            lblCurrentCalculation.Text += "+";
                            Input.Text = String.Empty;
                    }
                    
                 }
            }else
            {
                if (Char.IsNumber(Input.Text[Input.TextLength - 1]))
                {
                    //Podstawapercent zapamietuje liczbe z ktorej ma liczyc pozniej procent
                    podstawapercent = decimal.Parse(Input.Text);
                    wynik = decimal.Parse(Input.Text);
                    Input.Text = String.Empty;
                    ostatniaAkcja = "+";
                    lblCurrentCalculation.Text += "+";
                }
                
            }
           
        }

        private void btnMinus_Click(object sender, EventArgs e)
        {
            ++klikniecia;
            if (klikniecia > 1)
            {
                if (string.IsNullOrWhiteSpace(Input.Text))
                {

                }
                else
                {
                    if (Char.IsNumber(Input.Text[Input.TextLength - 1]))
                    {
                        //Podstawapercent zapamietuje liczbe z ktorej ma liczyc pozniej procent
                        podstawapercent = decimal.Parse(Input.Text);
                        wynik -= decimal.Parse(Input.Text);
                        label1.Text = wynik.ToString();
                        ostatniaAkcja = "-";
                        lblCurrentCalculation.Text += "-";
                        Input.Text = String.Empty;

                    }

                }
            }
            else
            {
                if (Char.IsNumber(Input.Text[Input.TextLength - 1]))
                {
                    //Podstawapercent zapamietuje liczbe z ktorej ma liczyc pozniej procent
                    podstawapercent = decimal.Parse(Input.Text);
                    wynik = decimal.Parse(Input.Text);
                    Input.Text = String.Empty;
                    ostatniaAkcja = "-";
                    lblCurrentCalculation.Text += "-";
                }

            }

        }

        private void btnMultiply_Click(object sender, EventArgs e)
        {
            ++klikniecia;
            if (klikniecia > 1)
            {
                if (string.IsNullOrWhiteSpace(Input.Text))
                {

                }
                else
                {
                    if (Char.IsNumber(Input.Text[Input.TextLength - 1]))
                    {
                        //Podstawapercent zapamietuje liczbe z ktorej ma liczyc pozniej procent
                        podstawapercent = decimal.Parse(Input.Text);
                        wynik *= decimal.Parse(Input.Text);
                        label1.Text = wynik.ToString();
                        ostatniaAkcja = "*";
                        lblCurrentCalculation.Text += "*";
                        Input.Text = String.Empty;

                    }

                }
            }
            else
            {
                if (Char.IsNumber(Input.Text[Input.TextLength - 1]))
                {
                    //Podstawapercent zapamietuje liczbe z ktorej ma liczyc pozniej procent
                    podstawapercent = decimal.Parse(Input.Text);
                    wynik = decimal.Parse(Input.Text);
                    Input.Text = String.Empty;
                    ostatniaAkcja = "*";
                    lblCurrentCalculation.Text += "*";
                }

            }

        }

        private void btnDivide_Click(object sender, EventArgs e)
        {
            ++klikniecia;
            if (klikniecia > 1)
            {
                if (string.IsNullOrWhiteSpace(Input.Text))
                {

                }
                else
                {
                    if (Char.IsNumber(Input.Text[Input.TextLength - 1]))
                    {
                        //Podstawapercent zapamietuje liczbe z ktorej ma liczyc pozniej procent
                        podstawapercent = decimal.Parse(Input.Text);
                        wynik /= decimal.Parse(Input.Text);
                        label1.Text = wynik.ToString();
                        ostatniaAkcja = "/";
                        lblCurrentCalculation.Text += "/";
                        Input.Text = String.Empty;

                    }

                }
            }
            else
            {
                if (Char.IsNumber(Input.Text[Input.TextLength - 1]))
                {
                    //Podstawapercent zapamietuje liczbe z ktorej ma liczyc pozniej procent
                    podstawapercent = decimal.Parse(Input.Text);
                    wynik = decimal.Parse(Input.Text);
                    Input.Text = String.Empty;
                    ostatniaAkcja = "/";
                    lblCurrentCalculation.Text += "/";
                }

            }

           

        }
        private void btnPercent_Click(object sender, EventArgs e)
        {
            decimal percent = 1;
            if (ostatniaAkcja == "+")
            {
                percent = (decimal.Parse(Input.Text) * 0.01M) * podstawapercent;
                Input.Text = percent.ToString();
                //MessageBox.Show(percent.ToString());
            }else if (ostatniaAkcja == "-")
            {

            }else if (ostatniaAkcja == "*")
            {
                percent = (decimal.Parse(Input.Text) * 0.01M) * podstawapercent;
                MessageBox.Show(percent.ToString());
            }
            else
            {

            }
            
        }
        #endregion
        #region "Liczby i przecinek"
        private void btn1_Click(object sender, EventArgs e)
        {
            Input.Text += "1";
            lblCurrentCalculation.Text += "1";
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            Input.Text += "2";
            lblCurrentCalculation.Text += "2";
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            Input.Text += "3";
            lblCurrentCalculation.Text += "3";
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            Input.Text += "4";
            lblCurrentCalculation.Text += "4";
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            Input.Text += "5";
            lblCurrentCalculation.Text += "5";
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            Input.Text += "6";
            lblCurrentCalculation.Text += "6";
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            Input.Text += "7";
            lblCurrentCalculation.Text += "7";
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            Input.Text += "8";
            lblCurrentCalculation.Text += "8";
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            Input.Text += "9";
            lblCurrentCalculation.Text += "9";
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            Input.Text += "0";
            lblCurrentCalculation.Text += "0";
        }
        private void btnComma_Click(object sender, EventArgs e)
        {
            if (Char.IsNumber(Input.Text[Input.TextLength - 1]))
            {
                Input.Text += ",";
            }
            
        }
        #endregion
#region "Wynik dzialania"
        private void btnResult_Click_1(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(Input.Text))
            {
                
            }else
            {
                if (ostatniaAkcja == "+")
                {
                    wynik += decimal.Parse(Input.Text);
                    label1.Text = wynik.ToString();
                    //Input.Text = String.Empty;
                    //wynik = 0;
                    //klikniecia = 0;
                    lblCurrentCalculation.Text = "";
                }
                else if (ostatniaAkcja == "-")
                {
                    wynik -= decimal.Parse(Input.Text);
                    label1.Text = wynik.ToString();
                    Input.Text = String.Empty;
                    wynik = 0;
                    klikniecia = 0;
                    lblCurrentCalculation.Text = "";
                }
                else if (ostatniaAkcja == "*")
                {
                    wynik *= decimal.Parse(Input.Text);
                    label1.Text = wynik.ToString();
                    Input.Text = String.Empty;
                    wynik = 0;
                    klikniecia = 0;
                    lblCurrentCalculation.Text = "";
                }
                else
                {
                    wynik /= decimal.Parse(Input.Text);
                    label1.Text = wynik.ToString();
                    Input.Text = String.Empty;
                    wynik = 0;
                    klikniecia = 0;
                    lblCurrentCalculation.Text = "";
                }
            }
            
        }

        private void btnPlusMinus_Click(object sender, EventArgs e)
        {
            decimal inputliczba;
            inputliczba = decimal.Parse(Input.Text);
            if (Input.Text.Contains("-"))
            {
                //Input.Text = string.Empty;
                //Input.Text = inputliczba.ToString();
                //Input.Text.Replace("-", "");
                StringBuilder sb = new StringBuilder(Input.Text);
                sb.Remove(0, 1);
                Input.Text = sb.ToString();
            }
            else
            {
                Input.Text = string.Empty;
                Input.Text = "-" + inputliczba.ToString();
            }
        }


        #endregion

        #region "Usuwanie tekstu"
        private void btnCE_Click(object sender, EventArgs e)
        {
            if (Input.TextLength > 0)
                {
                    Input.Text = Input.Text.Remove(Input.TextLength - 1);
                }
                
        }
        private void btnC_Click(object sender, EventArgs e)
        {
            Input.Text = String.Empty;
            label1.Text = String.Empty;
            wynik = 0;
            lblCurrentCalculation.Text = String.Empty;
            podstawapercent = 0;
        }
        #endregion
#region "Nieuzywane"
        private void Form1_Load(object sender, EventArgs e)
        {

        }
        #endregion

    }
}
